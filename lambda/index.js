/* *
 * This sample demonstrates handling intents from an Alexa skill using the Alexa Skills Kit SDK (v2).
 * Please visit https://alexa.design/cookbook for additional examples on implementing slots, dialog management,
 * session persistence, api calls, and more.
 * */
const Alexa = require('ask-sdk-core');
const { getHumidify, getTemperature, postTurn,} = require('./services/routes');

const LaunchRequestHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === "LaunchRequest"
    );
  },
  handle(handlerInput) {
    const responses = [
      "Central de umidificação ativada, olá como posso ajudar?",
      "Olá, a central de umidificação está pronta. Em que posso auxiliar?",
      "Oi, a central de umidificação está pronta. Em que posso ajudar?",
      "Ei, a central de umidificação está pronta. Como posso ajudar?",
    ];
    const speakOutput = responses[Math.floor(Math.random() * responses.length)];
    return handlerInput.responseBuilder
      .speak(speakOutput)
      .reprompt(speakOutput)
      .getResponse();
  },
};

const TurnOnHumidifierIntentHandler = {
  canHandle(handlerInput) {
      return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
          && Alexa.getIntentName(handlerInput.requestEnvelope) === 'TurnOnHumidifierIntent';
  },
  handle(handlerInput) {
      const isSuccessful = postTurn();

      const successResponses = [
        "Umidificador ligado e funcionando!",
        "Pronto, o umidificador está ativo agora.",
        "O umidificador foi ligado com sucesso.",
        "A umidificação do ambiente começou.",
        "Umidificador ativado. Ajustando a umidade do ambiente."
      ];

      const failureResponses = [
        "Desculpe, não consegui ligar o umidificador agora.",
        "Parece que houve um problema ao ativar o umidificador.",
        "Não estou conseguindo ligar o umidificador no momento.",
        "Ops, algo deu errado e o umidificador não foi ativado.",
        "Infelizmente, não posso ligar o umidificador agora."
      ];
      let speakOutput;
      if (isSuccessful) {
          speakOutput = successResponses[Math.floor(Math.random() * successResponses.length)];
      } 
      else {
          speakOutput = failureResponses[Math.floor(Math.random() * failureResponses.length)];
      }

      return handlerInput.responseBuilder
          .speak(speakOutput)
          .getResponse();
  }
};

const CheckHumidityIntentHandler = {
  canHandle(handlerInput) {
      return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
          && Alexa.getIntentName(handlerInput.requestEnvelope) === 'CheckHumidityIntent';
  },
  handle(handlerInput) {
      const humidify = getHumidify();

      let speakOutput;
      if (humidify === null) {
        speakOutput = `Desculpe, não consegui verificar a umidade no momento`;
      } else if (humidify < 30) {
          const lowHumidityResponses = [
            `Tá bem seco aqui, só ${humidify}% de umidade.`,
            `Poxa, só temos ${humidify}% de umidade. Precisa aumentar isso aí.`,
            `Está bem baixo aqui, a umidade está em apenas ${humidify}%.`,
            `Que secura! Estamos com ${humidify}% de umidade agora.`,
            `Está precisando de umidade aqui, está em ${humidify}%.`
          ];
          speakOutput = lowHumidityResponses[Math.floor(Math.random() * lowHumidityResponses.length)];
      } else if (humidify >= 30 && humidify <= 60) {
          const idealHumidityResponses = [
            `Tá tudo certo por aqui, a umidade está em ${humidify}%.`,
            `A umidade tá boa, estamos com ${humidify}%.`,
            `Tá ideal aqui, ${humidify}% de umidade.`,
            `A umidade está perfeita, ${humidify}%.`,
            `Está tudo equilibrado, a umidade está em ${humidify}%.`
          ];
          speakOutput = idealHumidityResponses[Math.floor(Math.random() * idealHumidityResponses.length)];
      } else if (humidify > 60) {
          const highHumidityResponses = [
            `Tá um pouco úmido aqui, com ${humidify}% de umidade.`,
            `A umidade está alta, estamos com ${humidify}%.`,
            `Parece que está úmido demais aqui, ${humidify}% de umidade.`,
            `Umidade acima do normal, está em ${humidify}%.`,
            `Tá bem úmido, a umidade está em ${humidify}%.`
          ];
          speakOutput = highHumidityResponses[Math.floor(Math.random() * highHumidityResponses.length)];
      } else {
          speakOutput = `A umidade está em ${humidify}%.`; // Resposta genérica para qualquer outro caso
      }

      return handlerInput.responseBuilder
          .speak(speakOutput)
          .getResponse();
  }
};


const CheckTemperatureIntentHandler = {
  canHandle(handlerInput) {
      return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
          && Alexa.getIntentName(handlerInput.requestEnvelope) === 'CheckTemperatureIntent';
  },
  handle(handlerInput) {
      const temperature = getTemperature(); // Supõe-se que retorna um valor numérico da temperatura

      let speakOutput;
      if (temperature === null) {
        speakOutput = `Desculpe, não consegui verificar a temperatura no momento.`;
      } else if (temperature < 10) {
          const coldResponses = [
            `Tá bem frio aqui, só ${temperature} graus.`,
            `Puxa, está congelando com ${temperature} graus.`,
            `Vish, tá um gelo, estamos com ${temperature} graus.`,
            `Melhor pegar um casaco, está ${temperature} graus aqui.`,
            `Que friaca! Estamos com apenas ${temperature} graus.`
          ];
          speakOutput = coldResponses[Math.floor(Math.random() * coldResponses.length)];
      } else if (temperature >= 10 && temperature <= 25) {
          const mildResponses = [
            `Tá uma temperatura agradável, ${temperature} graus.`,
            `Clima bem ameno, estamos com ${temperature} graus.`,
            `Nada mal por aqui, ${temperature} graus.`,
            `Temperatura bem confortável, ${temperature} graus.`,
            `Clima legal, está ${temperature} graus agora.`
          ];
          speakOutput = mildResponses[Math.floor(Math.random() * mildResponses.length)];
      } else if (temperature > 25) {
          const hotResponses = [
            `Tá quente por aqui, ${temperature} graus.`,
            `Nossa, está um forno com ${temperature} graus.`,
            `Muito calor, estamos com ${temperature} graus.`,
            `Parece verão, ${temperature} graus agora.`,
            `Tá pegando fogo, ${temperature} graus aqui.`
          ];
          speakOutput = hotResponses[Math.floor(Math.random() * hotResponses.length)];
      } else {
          speakOutput = `A temperatura está em ${temperature} graus.`; // Resposta genérica para qualquer outro caso
      }

      return handlerInput.responseBuilder
          .speak(speakOutput)
          .getResponse();
  }
};

// AtiveForAMinuteIntent
const AtiveForAMinuteIntentHandler = {
  canHandle(handlerInput) {
    return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
        && Alexa.getIntentName(handlerInput.requestEnvelope) === 'AtiveForAMinuteIntent';
  },
  handle(handlerInput) {
    const isSuccessful = activateHumidifierForOneMinute();
    const successResponses = [
      "Umidificador ativado por um minuto!",
      "Ok, o umidificador vai funcionar por um minutinho.",
      "Tudo certo, umidificador ligado por um minuto.",
      "Coloquei o umidificador para rodar por um minuto.",
      "Pronto, umidificador ativado por um minuto. Aproveite!"
    ];
    const failureResponses = [
      "Não consegui ativar o umidificador por um minuto agora.",
      "Ops, algo deu errado e não pude ligar o umidificador por um minuto.",
      "Parece que não posso ativar o umidificador por um minuto no momento.",
      "Infelizmente, não foi possível ligar o umidificador por um minuto agora.",
      "Desculpe, mas não posso ativar o umidificador por um minuto."
    ];

    const speakOutput = isSuccessful
      ? successResponses[Math.floor(Math.random() * successResponses.length)]
      : failureResponses[Math.floor(Math.random() * failureResponses.length)];

    return handlerInput.responseBuilder
      .speak(speakOutput)
      .getResponse();
  }
};

const ScheduleHumidifierIntentHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === "IntentRequest" &&
      Alexa.getIntentName(handlerInput.requestEnvelope) === "ScheduleHumidifierIntent"
    );
  },
  handle(handlerInput) {
    const init_time = handlerInput.requestEnvelope.request.intent.slots.raw_init_time.value;
    const end_time = handlerInput.requestEnvelope.request.intent.slots.raw_end_time.value;

    const isScheduledSuccessfully = scheduleHumidifier(init_time, end_time);

    const successResponses = [
      `Tudo certo, umidificador programado das ${init_time} às ${end_time}.`,
      `Ok, vou manter o umidificador ligado das ${init_time} até as ${end_time}.`,
      `Programado! O umidificador vai funcionar das ${init_time} às ${end_time}.`,
      `Horário ajustado: umidificador ativo das ${init_time} até as ${end_time}.`,
      `Pronto, agendado para funcionar das ${init_time} às ${end_time}.`
    ];

    const failureResponses = [
      `Não consegui programar o umidificador para as horas escolhidas.`,
      `Parece que houve um problema ao agendar das ${init_time} às ${end_time}.`,
      `Não foi possível ajustar o umidificador para funcionar das ${init_time} até as ${end_time}.`,
      `Ops, algo deu errado. Não pude agendar o umidificador para essas horas.`,
      `Infelizmente, não posso configurar o umidificador para operar das ${init_time} às ${end_time} agora.`
    ];

    const speakOutput = isScheduledSuccessfully
      ? successResponses[Math.floor(Math.random() * successResponses.length)]
      : failureResponses[Math.floor(Math.random() * failureResponses.length)];

    return handlerInput.responseBuilder
      .speak(speakOutput)
      .reprompt(speakOutput)
      .getResponse();
  },
};


const turnOnUmidifierBeneathHumidityPercentageIntentHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === "IntentRequest" &&
      Alexa.getIntentName(handlerInput.requestEnvelope) ===
        "turnOnUmidifierBeneathHumidityPercentageIntentHandler"
    );
  },
  handle(handlerInput) {
    const percentage = handlerInput.requestEnvelope.request.intent.slots.percentage.value;
    const minutes = handlerInput.requestEnvelope.request.intent.slots.minutes.value;

    const isConfiguredSuccessfully = configureHumidifier(percentage, minutes);

    const successResponses = [
      `Umidificador configurado para ser ativado a cada ${minutes} minutos, quando a umidade estiver abaixo de ${percentage}%.`,
      `Pronto! Vou ligar o umidificador a cada ${minutes} minutos se estiver menos úmido que ${percentage}%.`,
      `Tudo ajustado. Se a umidade cair abaixo de ${percentage}%, ligo o umidificador por ${minutes} minutos.`,
      `Configuração feita! Menos de ${percentage}% de umidade e o umidificador entra em ação por ${minutes} minutos.`,
      `Ok, se a umidade estiver abaixo de ${percentage}%, o umidificador vai funcionar a cada ${minutes} minutos.`
    ];

    const failureResponses = [
      `Não consegui configurar o umidificador para ativar sob ${percentage}% a cada ${minutes} minutos.`,
      `Ops, algo deu errado. Não pude ajustar o umidificador para menos de ${percentage}% de umidade.`,
      `Infelizmente, não posso definir o umidificador para ligar a cada ${minutes} minutos em ${percentage}% de umidade.`,
      `Houve um problema. Não consegui configurar o umidificador para operar abaixo de ${percentage}% de umidade.`,
      `Desculpe, mas não consegui ajustar o umidificador para funcionar com menos de ${percentage}% de umidade.`
    ];

    const speakOutput = isConfiguredSuccessfully
      ? successResponses[Math.floor(Math.random() * successResponses.length)]
      : failureResponses[Math.floor(Math.random() * failureResponses.length)];

    return handlerInput.responseBuilder
      .speak(speakOutput)
      .reprompt(speakOutput)
      .getResponse();
  },
};


const CleanScheduleHumidifierIntent = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === "IntentRequest" &&
      Alexa.getIntentName(handlerInput.requestEnvelope) === "CleanScheduleHumidifierIntent"
    );
  },
  handle(handlerInput) {
    const isClearedSuccessfully = clearHumidifierConfigurations();

    const successResponses = [
      `Todas as configurações do umidificador foram apagadas.`,
      `Limpei todas as programações do umidificador pra você.`,
      `Pronto, nenhuma configuração restante no umidificador.`,
      `Todas as configurações do umidificador foram removidas com sucesso.`,
      `As configurações do umidificador? Apaguei todas elas.`
    ];

    const failureResponses = [
      `Não consegui apagar as configurações do umidificador.`,
      `Ops, algo deu errado. Não pude limpar as configurações do umidificador.`,
      `Infelizmente, não pude remover as configurações do umidificador agora.`,
      `Houve um problema ao tentar limpar as configurações do umidificador.`,
      `Desculpe, mas não consegui apagar as configurações do umidificador.`
    ];

    const speakOutput = isClearedSuccessfully
      ? successResponses[Math.floor(Math.random() * successResponses.length)]
      : failureResponses[Math.floor(Math.random() * failureResponses.length)];

    return handlerInput.responseBuilder
      .speak(speakOutput)
      .reprompt(speakOutput)
      .getResponse();
  },
};


const HelpIntentHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === "IntentRequest" &&
      Alexa.getIntentName(handlerInput.requestEnvelope) === "AMAZON.HelpIntent"
    );
  },
  handle(handlerInput) {
    const helpResponses = [
      "Como posso te ajudar? Você pode pedir para verificar a umidade, verificar a temperatura ou programar o umidificador.",
      "Estou aqui para ajudar. Diga coisas como 'Qual a umidade agora?' ou 'Ligue o umidificador.'",
      "Precisa de uma mãozinha? Eu posso informar a temperatura, a umidade, ligar umidificador ou mudar as configurações do umidificador.",
      "Posso ajudar com várias coisas, como checar a umidade ou ligar o umidificador. O que você precisa?",
      "Estou aqui para ajudar! Diga o que você precisa, como 'Defina o umidificador para 30 minutos' ou 'Qual a temperatura?' ou 'Qual a umidade?'"
    ];

    const speakOutput = helpResponses[Math.floor(Math.random() * helpResponses.length)];

    return handlerInput.responseBuilder
      .speak(speakOutput)
      .reprompt(speakOutput)
      .getResponse();
  },
};


const CancelAndStopIntentHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === "IntentRequest" &&
      (Alexa.getIntentName(handlerInput.requestEnvelope) ===
        "AMAZON.CancelIntent" ||
        Alexa.getIntentName(handlerInput.requestEnvelope) ===
          "AMAZON.StopIntent")
    );
  },
  handle(handlerInput) {
    const speakOutput = "Tchau, até a próxima!";
    return handlerInput.responseBuilder.speak(speakOutput).getResponse();
  },
};
/* *
 * FallbackIntent triggers when a customer says something that doesn’t map to any intents in your skill
 * It must also be defined in the language model (if the locale supports it)
 * This handler can be safely added but will be ingnored in locales that do not support it yet
 * */
const FallbackIntentHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === "IntentRequest" &&
      Alexa.getIntentName(handlerInput.requestEnvelope) ===
        "AMAZON.FallbackIntent"
    );
  },
  handle(handlerInput) {
    const speakOutput =
      "Desculpe, eu não sei sobre isto. Por favor, tente novamente.";

    return handlerInput.responseBuilder
      .speak(speakOutput)
      .reprompt(speakOutput)
      .getResponse();
  },
};

// HowCanIActiveHumidifierIntent
const HowCanIActiveHumidifierIntentHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === "IntentRequest" &&
      Alexa.getIntentName(handlerInput.requestEnvelope) === "HowCanIActiveHumidifierIntent"
    );
  },
  handle(handlerInput) {
    // Lista de instruções detalhadas
    const instructions = [
      `Para ligar o umidificador, comece dizendo: Alexa, abra a central de umidificação. Depois, para ativá-lo, diga: Alexa, ative o umidificador. Você pode também definir um tempo específico, como: Alexa, ative o umidificador por cinco minutos.`,
      `Primeiro, peça para abrir a central de umidificação com: Alexa, abra a central de umidificação. Em seguida, você pode ligar o umidificador falando: Alexa, ative o umidificador. Se quiser definir um tempo, é só dizer: Alexa, ative o umidificador por dez minutos.`,
      `Você precisa primeiro abrir a central de umidificação. Diga: Alexa, abra a central de umidificação. Depois, para ligar o umidificador, basta dizer: Alexa, ative o umidificador. Se desejar, especifique um tempo como: Alexa, ative o umidificador por três minutos.`,
      `Abra a central de umidificação com o comando: Alexa, abra a central de umidificação. Então, para ativar o umidificador, diga: Alexa, ative o umidificador. Você também pode definir um período específico, por exemplo: Alexa, ative o umidificador por dois minutos.`,
      `Comece ativando a central de umidificação com: Alexa, abra a central de umidificação. Após isso, para ligar o umidificador, diga: Alexa, ative o umidificador. Para um tempo específico, use: Alexa, ative o umidificador por quinze minutos.`
    ];

    const speakOutput = instructions[Math.floor(Math.random() * instructions.length)];

    return handlerInput.responseBuilder
      .speak(speakOutput)
      .reprompt(speakOutput)
      .getResponse();
  },
};


// WitchCommandsCanIAskForHumidifierIntent
const WitchCommandsCanIAskForHumidifierIntentHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === "IntentRequest" &&
      Alexa.getIntentName(handlerInput.requestEnvelope) === "WitchCommandsCanIAskForHumidifierIntent"
    );
  },
  handle(handlerInput) {
    const capabilitiesDescriptions = [
      `Você pode perguntar sobre a temperatura e a umidade atuais, ativar o umidificador por um período definido, configurá-lo para ligar automaticamente quando a umidade estiver baixa, e também definir horários específicos para ele funcionar.`,
      `O umidificador pode informar a temperatura atual e o nível de umidade, ser ativado por um tempo que você escolher, ligar automaticamente se a umidade estiver abaixo de um certo nível, ou operar em horários que você definir.`,
      `Com o umidificador, você pode verificar a temperatura e a umidade do ambiente, ativar o aparelho por um minuto ou por um tempo à sua escolha, configurá-lo para funcionar automaticamente em baixa umidade, e programá-lo para horários específicos.`,
      `Você tem várias opções com o umidificador: verificar as condições atuais de temperatura e umidade, ativar o aparelho por um minuto, programá-lo para ligar em umidade baixa, ou definir horários específicos para ele operar.`,
      `Com este umidificador, você pode saber a temperatura e a umidade, ligá-lo por um tempo determinado, configurá-lo para responder a umidade baixa, ou estabelecer horários específicos para ele funcionar.`
    ];

    const speakOutput = capabilitiesDescriptions[Math.floor(Math.random() * capabilitiesDescriptions.length)];

    return handlerInput.responseBuilder
      .speak(speakOutput)
      .reprompt(speakOutput)
      .getResponse();
  },
};


const HowDoesAlexaCommunicateWithTheHumidifierIntentHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === "IntentRequest" &&
      Alexa.getIntentName(handlerInput.requestEnvelope) === "HowDoesAlexaCommunicateWithTheHumidifierIntent"
    );
  },
  handle(handlerInput) {
    const communicationExplanations = [
      `A Alexa usa a internet para enviar comandos a um servidor web por HTTP. Depois, esse servidor fala com o umidificador usando o protocolo MQTT.`,
      `Primeiro, a Alexa manda informações pela internet usando HTTP até um servidor. Esse servidor então se comunica com o umidificador através do MQTT.`,
      `A comunicação começa com a Alexa enviando comandos via HTTP para um servidor na web. Em seguida, o servidor transmite esses comandos ao umidificador usando o protocolo MQTT.`,
      `A Alexa fala com um servidor web usando o protocolo HTTP. Depois, o servidor usa o MQTT para enviar os comandos ao umidificador.`,
      `A Alexa se comunica com um servidor web usando o protocolo HTTP, e o servidor web se comunica com o umidificador usando o protocolo MQTT. É assim que os seus comandos chegam até ele.`
    ];

    const speakOutput = communicationExplanations[Math.floor(Math.random() * communicationExplanations.length)];

    return handlerInput.responseBuilder
      .speak(speakOutput)
      .reprompt(speakOutput)
      .getResponse();
  },
};


// 

/* *
 * SessionEndedRequest notifies that a session was ended. This handler will be triggered when a currently open
 * session is closed for one of the following reasons: 1) The user says "exit" or "quit". 2) The user does not
 * respond or says something that does not match an intent defined in your voice model. 3) An error occurs
 * */
const SessionEndedRequestHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) ===
      "SessionEndedRequest"
    );
  },
  handle(handlerInput) {
    console.log(
      `~~~~ Session ended: ${JSON.stringify(handlerInput.requestEnvelope)}`,
    );
    // Any cleanup logic goes here.
    return handlerInput.responseBuilder.getResponse(); // notice we send an empty response
  },
};
/* *
 * The intent reflector is used for interaction model testing and debugging.
 * It will simply repeat the intent the user said. You can create custom handlers for your intents
 * by defining them above, then also adding them to the request handler chain below
 * */
const IntentReflectorHandler = {
  canHandle(handlerInput) {
    return (
      Alexa.getRequestType(handlerInput.requestEnvelope) === "IntentRequest"
    );
  },
  handle(handlerInput) {
    const intentName = Alexa.getIntentName(handlerInput.requestEnvelope);
    const speakOutput = `Você acionou a ação ${intentName}`;

    return (
      handlerInput.responseBuilder
        .speak(speakOutput)
        //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
        .getResponse()
    );
  },
};
/**
 * Generic error handling to capture any syntax or routing errors. If you receive an error
 * stating the request handler chain is not found, you have not implemented a handler for
 * the intent being invoked or included it in the skill builder below
 * */
const ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    const speakOutput =
      "Desculpe, tive um problema ao tentar executar a ação pedida. Por favor, tente novamente.";
    console.log(`~~~~ Error handled: ${JSON.stringify(error)}`);

    return handlerInput.responseBuilder
      .speak(speakOutput)
      .reprompt(speakOutput)
      .getResponse();
  },
};

/**
 * This handler acts as the entry point for your skill, routing all request and response
 * payloads to the handlers above. Make sure any new handlers or interceptors you've
 * defined are included below. The order matters - they're processed top to bottom
 * */
exports.handler = Alexa.SkillBuilders.custom()
  .addRequestHandlers(
    LaunchRequestHandler,
    TurnOnHumidifierIntentHandler,
    CheckHumidityIntentHandler,
    AtiveForAMinuteIntentHandler,
    CheckTemperatureIntentHandler,
    HelpIntentHandler,
    CancelAndStopIntentHandler,
    FallbackIntentHandler,
    SessionEndedRequestHandler,
    ScheduleHumidifierIntentHandler,
    turnOnUmidifierBeneathHumidityPercentageIntentHandler,
    CleanScheduleHumidifierIntent,
    IntentReflectorHandler,
    HowCanIActiveHumidifierIntentHandler,
    WitchCommandsCanIAskForHumidifierIntentHandler,
    HowDoesAlexaCommunicateWithTheHumidifierIntentHandler,
  )
  .addErrorHandlers(ErrorHandler)
  .withCustomUserAgent("sample/hello-world/v1.2")
  .lambda();
