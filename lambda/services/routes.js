const axios = require('axios');

const baseUrl = 'URL'; // Substitua 'URL' pela URL real do seu servidor

async function getHumidify() {
  try {
    const response = await axios.get(`${baseUrl}/humidify`);
    return response.data;
  } catch (error) {
    throw error;
  }
}

async function getTemperature() {
  try {
    const response = await axios.get(`${baseUrl}/temperature`);
    return response.data;
  } catch (error) {
    throw error;
  }
}

async function postTurn(data) {
  try {
    const response = await axios.post(`${baseUrl}/turn`, data);
    return response.data;
  } catch (error) {
    throw error;
  }
}

module.exports = {
  getHumidify,
  getTemperature,
  postTurn,
};